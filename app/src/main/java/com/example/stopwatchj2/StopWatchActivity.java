package com.example.stopwatchj2;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

public class StopWatchActivity extends Activity {

    private int seconds = 0;
    private boolean running;
    private boolean wasRunning;

    @Override
    protected void onCreate(Bundle savedfuckenInstanceState) {
        super.onCreate(savedfuckenInstanceState);
        setContentView(R.layout.activity_stop_watch);
        if (savedfuckenInstanceState != null){
            seconds = savedfuckenInstanceState.getInt("seconds");
            running = savedfuckenInstanceState.getBoolean("running");
            wasRunning = savedfuckenInstanceState.getBoolean(("wasRunning"));
        }
        runTimer();
    }
    private void runTimer() {
        final TextView timeView = (TextView)findViewById(R.id.time_view);
        final Handler boxx = new Handler(); // Handler используется для планирования выполнения или передачи кода другому потоку.
        boxx.post(new Runnable() {
            @Override
            public void run() {
                int hours = seconds/3600;
                int minutes = (seconds%3600)/60;
                int secs = seconds%60;
                String time = String.format(Locale.getDefault(),
                        "%d:%02d:%02d", hours, minutes, secs);
                timeView.setText(time);
                if (running) { seconds++;
                }
                boxx.postDelayed(this, 1000);
            }
        });
    }

    public void onClickStart(View view) {
        running = true;
    }

    public void onClickStop(View view) {
        running = false;
    }

    public void onClickReset(View view) {
        running = false;
        seconds = 0;
    }

    @Override
    public void onSaveInstanceState (Bundle savedfuckenInstanceState) {
        savedfuckenInstanceState.putInt("seconds", seconds);
        savedfuckenInstanceState.putBoolean("running", running);
        savedfuckenInstanceState.putBoolean("wasRunning", wasRunning);
    }

    @Override
    protected void onPause() {
        super.onPause();
        wasRunning = running;
        running = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (wasRunning) {
        running = true;
        }
    }
}